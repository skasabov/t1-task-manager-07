package ru.t1.skasabov.tm;

import ru.t1.skasabov.tm.constant.ArgumentConst;
import ru.t1.skasabov.tm.constant.CommandConst;
import ru.t1.skasabov.tm.util.FormatUtil;

import java.util.Scanner;

public final class Application {

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.INFO:
                showSystemInfo();
                break;
            case CommandConst.EXIT:
                showExit();
                break;
            default:
                showErrorCommand();
                break;
        }
    }

    private static void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            default:
                showErrorArgument();
                break;
        }
        System.exit(0);
    }

    private static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = FormatUtil.format(freeMemory);
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory));
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = FormatUtil.format(totalMemory);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.format(usageMemory);
		System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void showErrorArgument() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Stas Kasabov");
        System.out.println("email: stas@kasabov.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show developer info.\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show application version.\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show command list.\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s, %s - Show system info.\n", CommandConst.INFO, ArgumentConst.INFO);
        System.out.printf("%s - Close application.\n", CommandConst.EXIT);
    }

}
